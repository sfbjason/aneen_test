import Vue from "vue";

export const ADD_NEW_ADDRESSBOOK = (state, payload) => {
  state.address_book.push(payload);
  state.cache_address_book = state.address_book;
};

export const SET_ON_UPDATE = (state, payload) => {
  state.is_on_update = payload;
};

export const SET_ROW_INDEX = (state, payload) => {
  state.selected_row_index = payload;
};

export const UPDATE_ROW_DATA = (state, payload) => {
  Vue.set(state.address_book, state.selected_row_index, payload);
};

export const UPDATE_CACHE_DATA = (state, payload) => {
  state.cache_address_book = payload;
};
