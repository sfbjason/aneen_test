export const addNewItem = ({ commit }, payload) => {
  commit("ADD_NEW_ADDRESSBOOK", payload);
};

export const setOnUpdate = ({ commit }, payload) => {
  commit("SET_ON_UPDATE", payload);
};

export const setRowIndex = ({ commit }, payload) => {
  commit("SET_ROW_INDEX", payload);
};

export const updateRow = ({ commit }, payload) => {
  commit("UPDATE_ROW_DATA", payload);
};

export const filter_list = ({ commit, state }, payload) => {
  console.log(payload.options_selected.length);
  if (payload.email === "" && !payload.options_selected.length) {
    commit("UPDATE_CACHE_DATA", state.address_book);
  } else {
    const cache_address_book = [...state.cache_address_book];
    let filter_email = [];
    let filter_group = [];
    if (payload.email !== "") {
      filter_email = cache_address_book.filter(
        (row) => row.first_name === payload.email
      );
    }

    if (payload.options_selected.length) {
      filter_group = cache_address_book.filter((row) =>
        row.group.includes(payload.options_selected)
      );
    }

    let name = new Set(
      filter_email.map((d) => `${d.first_name} ${d.last_name}`)
    );
    var filted_data = [
      ...filter_email,
      ...filter_group.filter(
        (d) => !name.has(`${d.first_name} ${d.last_name}`)
      ),
    ];
    commit("UPDATE_CACHE_DATA", filted_data);
  }
};
