export const addressBooks = (state) => state.address_book;

export const isOnUpdate = (state) => state.is_on_update;

export const rowIndex = (state) => state.selected_row_index;

export const cached_address_book = (state) => state.cache_address_book;
