import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);

import form_store from "./form";

export default new Vuex.Store({
  modules: {
    form_store,
  },
});
