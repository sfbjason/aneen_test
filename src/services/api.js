import axios from "axios";
import MockAdapter from "axios-mock-adapter";

export default new (class FormService {
  getGroupOptions() {
    const mock = new MockAdapter(axios);
    const group = ["Family", "Friend", "Coworker"];

    mock.onGet("/options").reply(200, group);

    return axios.get("/options").then(function(response) {
      return response.data;
    });
  }
})();
